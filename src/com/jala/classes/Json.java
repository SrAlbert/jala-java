package com.jala.classes;

public class Json{
    public int errorCode;
    public String msg;
    public String token;

    public Json() {
        this.errorCode= 0;
        this.msg= "";
        this.token= "";
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getMsg() {
        return msg;
    }

    public String getToken() {
        return token;
    }
}