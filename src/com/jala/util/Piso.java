/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jala.util;

/**
 *
 * @author luks
 */
public class Piso {

    private final int referencia;
    private final String tipo;
    private final String operacióN;
    private final String provincia;
    private final int superficie;
    private final int precioVenta;
    private final String vendedor;

    public Piso(int referencia, String tipo, String operacion,
            String provincia, int superficie, int precioVenta,
            String vendedor) {
        this.referencia = referencia;
        this.tipo = tipo;
        this.operacióN = operacion;
        this.provincia = provincia;
        this.superficie = superficie;
        this.precioVenta = precioVenta;
        this.vendedor = vendedor;
    }
}
