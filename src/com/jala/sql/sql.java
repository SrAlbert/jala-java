package com.jala.sql;

import java.sql.*;

public class sql {
    private Connection conexion;
    private final String driver= "com.mysql.jdbc.Driver";
    private final String bbdd = "jdbc:mysql://localhost:3306/jala?zeroDateTimeBehavior=convertToNull";
    private final String user= "root";
    private final String pass= "";
    
    public sql() {
        try {
            Class.forName(driver);
            conexion= DriverManager.getConnection(bbdd, user, pass);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Connection getConexion() {
        return conexion;
    }   
    
    public void setConexion(Connection conexion) {
        this.conexion = conexion;
    }    
    
    public ResultSet consultar(String sql) {
        ResultSet result;
        try {
            Statement sentencia = getConexion().createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
            result = sentencia.executeQuery(sql);
            sentencia.close();
            result.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }        
        return result;
    }
    
    public ResultSet prepararQuery(String query, String... ele) throws SQLException{
        PreparedStatement ps= conexion.prepareStatement(query);
        
        int pi= 1;
        
        for (int i = 0; i < ele.length; i++) {
            switch(ele[i]){
                case "String":
                    ps.setString(pi, String.valueOf(ele[i+1]));
                    break;
                case "int":
                    ps.setInt(pi, Integer.parseInt(ele[i+1]));
                    break;
            }
            i++;
            pi++;
        }
        return ps.executeQuery();
    }
}